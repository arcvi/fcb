library(readxl)
library(dplyr)
library(data.table)
library(stringr)
empreses = read_excel("data/empreses.xlsx") %>% data.table()
setnames(empreses, tolower(names(empreses)))

empreses[, municipi:= str_extract(direccio, "\\([A-Z(' ]*\\)\\.$")]
empreses[, municipi:= gsub("[^A-Z ]", "", municipi)]

empreses[direccio!="NA", .(direccio, municipi)][1:50]


unique(empreses[direccio!="NA" & restaurant=="1", .(nom, empresa, direccio)][order(runif(.N))], by=NULL)
# nom                    empresa                                                                   direccio
# 1:                COCERA YUSTE SERGIO     GRUPO LANCASTER BCN SL                                       CL TELLINAIRES NUMEROS 39-41 (GAVA). - restaurant
# 2: VALIENTE LLAURADO FRANCISCO JAVIER             C2V GESTION SL                           CL RONDA GENERAL MITRE 211, LOCAL 3 (BARCELONA). - restaurant - sociv viu sobre - sweetmama
# 3:                   ROMERO SOLA LUIS                  HILU20 SL                               RB PRINCIPAL NUM.122 (VILANOVA I LA GELTRU). - restaurant - mas que café - soci viu bcn
# 4:               BLANCO GARCIA XAVIER ESPAI DE JOCS L'AMETLLA SL CR C-17 KM 27, LOCAL 2-3 BAIXOS C.C. SANT JORDI (AMETLLA DEL VALLES (L')). - xx
# 5:               BORDAS TOGORES RAMON      TERRAZA MAREMAGNUM SL                                 CL MUNTANER Num.240 P.3 PTA.2 (BARCELONA). - restaurant - direccio te pinta casa soci (no informada)
# ---                                                                                                                                         
#   754:              REBORDOSA SEGUI ENRIC            BOGARDE 1912 SL                             CL GRAN DE SANT ANDREU, 255 LOCAL (BARCELONA). - restaurant versalles
# 755:              VALLET MONZO SANTIAGO         CACERES ZENTRUM SL                                   CL ARAGON Num.281 P.5 PTA.1 (BARCELONA). - te pinta de ser una casa, no coincideix amb soci
# 756:               BELENES CASAS GERARD  LLURITU GRUP BARCELONA SL                               CL TORRENT DE L'OLLA 16-18, 4 3 (BARCELONA). - restaurant
# 757:            AMATLLER MOLINERO ENRIC MEL, MENTA I EUCALIPTUS SL                                      CL SARDENYA 311, LOCAL 7 (BARCELONA). - no trobo rest a maps
# 758:             DIAZ HERNANDEZ JOAQUIN POR QUE TE QUIERO MUCHO SL                                         CL PAU CLARIS NUM.189 (BARCELONA). - Entrepanes Díaz



fcb[apellidos=="VALIENTE LLAURADO", .(nombre, apellidos, direccion)]


unique(empreses[direccio!="NA" & botiga=="1", .(nom, empresa, direccio)][order(runif(.N))], by=NULL)
# nom                  empresa                                              direccio
# 1:     ROMERO BOHER JAVIER     080 MOTOPROTECCIO SL       CL GRAN DE SANT ANDREU. 139, LOCAL (BARCELONA). 
# 2:      LLOBET SALUD DAVID     100PERCENT EUROPE SL            CL VALENCIA NUM.245 P.2 PTA.2 (BARCELONA).
# 3:  MATURANA URIARTE JAIME     100PERCENT EUROPE SL            CL VALENCIA NUM.245 P.2 PTA.2 (BARCELONA).
# 4:   MATURANA URIARTE JUAN     100PERCENT EUROPE SL            CL VALENCIA NUM.245 P.2 PTA.2 (BARCELONA).
# 5:      ESTEVE TRIAS JORDI         3C MAS I 2000 SL               CL RAFAEL CASANOVA, 40-A (VALLROMANES).
# ---                                                                                                       
#   247:     SOLER MASFERRER POL     WALL BOX CHARGERS SL CL SERRA DE L'ORDAL NUM.10 (SANT ANDREU DE LA BARCA).
# 248: BLABIA ESTERUELAS JORGE        WOET & PROTEIN SL          PG INDUSTRIAL LES PEDRERES,NAVE 5 (MONTGAT).
# 249:   CASANOVA GRACIA JORDI           WORLD WORMS SL                       CL ANGEL GUIMERA Num.24 (GAVA).
# 250: TALAVERA MARTINEZ RAMON XL PERRUQUERS MANRESA SL                    PS PERE III NUM.34 P.BX (MANRESA).
# 251:  MANCEBO ARQUED ANTONIO         YOGOLINE 2016 SL        CL COLL I PUJOL NUM.198 P.EN PTA.3 (BADALONA).


v = unique(empreses[direccio!="NA" & (botiga=="1" | restaurant=="1"), .(clientid=empresa, address=direccio, postal_code="")], by=NULL)
v = unique(empreses[direccio!="NA" & (botiga=="1" | restaurant=="1"), .(clientid=paste0("d-",empresa), address=paste0(empresa, " ", municipi), postal_code="")], by=NULL)


write.csv(
  v,
  'E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/code/geoloc/scraper/test_geoloc1.csv',
  fileEncoding='UTF-8') # Aquí acaba!

# ----

library(RSQLite)
library(dplyr)
con = dbConnect(drv=SQLite(), dbname="E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/code/geoloc/scraper/data.sqlite")
dbGetQuery(con,"SELECT clientid, lat, lon, google_types, google_address, postal_code, postal_code_address FROM scrapy_GeolocItem") %>% data.table -> scraped_clientid

scraped_clientid = scraped_clientid[grepl("xyz", clientid)]
scraped_clientid[is.na(google_types), google_types:= "NA"]

calle_numero = c('["street_address"]', '["premise"]', '["subpremise"]')
scraped_clientid[, redo:= as.numeric(is.na(google_types) | !google_types %in% calle_numero)]
scraped_clientid[, do:= as.numeric(is.na(google_types))]
summary(scraped_clientid)



ind_cp = fread('E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/data/ind_cp.csv')
ind_cp = unique(ind_cp, by = "cp")

scraped_clientid = merge(scraped_clientid[, .(id=gsub("xyz", "", clientid), lat, lon, mala_geoloc=redo, google_address, cp_google=postal_code,
                                              cp=as.numeric(postal_code_address))],
                         ind_cp[, .(cp, lat_cp=lat, lon_cp=lon)],
                         by = "cp", all.x=T)
scraped_clientid

scraped_clientid[, lat:=as.numeric(lat)]
scraped_clientid[, lon:=as.numeric(lon)]

scraped_clientid[, lat:= ifelse(mala_geoloc==1, lat_cp, lat)]
scraped_clientid[, lon:= ifelse(mala_geoloc==1, lon_cp, lon)]

summary(scraped_clientid)

write.csv(scraped_clientid, "data/fcb_geoloc.csv", row.names=F)

scraped_clientid
# end ----
