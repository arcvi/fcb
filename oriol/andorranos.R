library(readxl)
library(dplyr)
library(data.table)
library(arcvitoolbox)

fcb = read_excel("data/FCB.xlsx") %>% data.table()
setnames(fcb, tolower(names(fcb)))

write.excel(fcb[ciudad=="ANDORRA LA VELLA", .(nombre, apellidos, direccion, ciudad, telefono, telefono2, email)])


