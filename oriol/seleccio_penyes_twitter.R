library(data.table)

# twitters = fread("data/twitters.csv")
twitters = read_delim("data/twitters.csv", delim=",", quote='"', locale=locale(encoding='UTF-8'))
setDT(twitters)

twitters[2, name]


twitters[, .(name, screen_name, clean_name, location, followers_count, friends_count)]
twitters[, mean(location=="Barcelona")]

twitters[, .N, by=location][order(-N)]

tw = twitters[grepl("pe((ny)|(ñ))a", tolower(name)) | grepl("pe((ny)|(ñ))a", tolower(screen_name)) | grepl("pe((ny)|(ñ))a", tolower(clean_name)),
              .(name, screen_name, clean_name, followers_count, friends_count)]

tw = unique(tw, by=NULL)
names_out = c("NachitoTV", "JavidelaPena", "ATLETINY", "fpsevillistas", "FPBeticas", "FPCadistas", "FPLeganes")
tw = tw[!screen_name %in% names_out]

# write.csv(tw, "data/twitters_penyes.csv")
saveRDS(tw, "data/twitters_penyes.rds")

