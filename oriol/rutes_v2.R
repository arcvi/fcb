# load ----
library(data.table)
library(dbscan)
library(sf)
library(stringr)

fcb = fread("data/fcb_full_join.csv")
fcb_geoloc = fread("data/fcb_geoloc.csv")
# saveRDS(fcb_geoloc,"data/fcb_geoloc.RDS")

fcb_geoloc = fcb_geoloc[!is.na(lon+lat) & mala_geoloc==0]

fcb = merge(fcb, fcb_geoloc[, .(id, lat, lon)], by="id")
fcb[, major50:= as.numeric(2020-any_cns) >= 50]

fcb[cod_mun=="8019", .N]
fcb = fcb[cod_mun=="8019", .(id, id1, lat, lon, major50)]

data1 = fread('E:/DOCS/Arcvi/Altres/arg/argentina/old/CDs/cens.csv')
data1[, id1:= 1:.N]

# data1[, .N, by=.(cp, cod_mun, municipi)][, .N, by=cp][order(-N)]
# data1[, .N, by=.(municipi, cod_mun)][, .N, by=cod_mun][order(-N)]
cp_muni = data1[, .N, by=.(cp, cod_mun, municipi)]
cp_muni[, r:=rank(-N, ties.method= "first"), by=cp]
cp_muni = cp_muni[r==1]
saveRDS(cp_muni, "data/cp_muni.RDS")


data1 = data1[cod_mun==8019]

fcb = merge(fcb, data1[, .(id1, carrer, num, municipi, cp, planta, porta)], by="id1")
fcb[, pis:= paste0(planta, " ", porta)]
fcb[, dir:= paste(carrer, num, ",", cp, municipi, sep=" ")]
fcb = unique(fcb, by="id1")

fcb_dir = fcb[, .(.N, lat=mean(lat), lon=mean(lon), majors50=sum(major50), portes=paste0(unique(pis), collapse="; ")), by=dir]

telfs = readRDS("data/telfs.rds")
telfs = telfs[business_status=="OPERATIONAL", .(N=1, lat, lon=long, majors50=1, portes="", dir=address, empresa=name)]
fcb_dir = rbind(fcb_dir, telfs, fill=T)

DT_sf = st_as_sf(fcb_dir, coords = c("lon", "lat"),
                 crs = 4326, agr = "constant") #4326 es el sistema lat lon de google

DT_utm = st_transform(DT_sf, 25831) # aixo es UTM31 són metres centrats a Barcelona mes o menys

DT_utm = data.table( st_coordinates(DT_utm) ) # aixo dona les coordenades en matriu per que les puguis ficar on vulguis

fcb_dir$x = DT_utm$X
fcb_dir$y = DT_utm$Y

fcb_dir = fcb_dir[!is.na(x+y)]

data1[, dir:= paste(carrer, num, ",", cp, municipi, sep=" ")]
data1 = data1[dir %in% fcb_dir$dir]

# routing ----
rutas = data.table()
max_t = 7200
id_ruta = 0
i = 0
temps = 0
vel = 3.4*1000/3600

fcb_dir[, done:= 0]
fcb_dir[, id:=1:.N]

estat = "new"
while (fcb_dir[done==0 & majors50>0, .N] > 0) {
  if (estat=="new") {
    print(i)
    i = i+1
    id_ruta = id_ruta + 1
    last_node = fcb_dir[done==0 & majors50>0][order(-N)][1, id]
    temps = 0
    estat = "ruta"
    rutas = rbind(rutas, data.table(id_ruta=id_ruta, node=last_node, x=fcb_dir[id==last_node, x], y=fcb_dir[id==last_node, y], temps=0))
    fcb_dir[id==last_node, done:=1]
  } else {
    fcb_dir$d_vec = sqrt((rutas[i,x] - fcb_dir$x)^2 + (rutas[i,y] - fcb_dir$y)^2)
    d_min = fcb_dir[done==0 & majors50>0][order(d_vec)][1, d_vec]
    the_min = fcb_dir[done==0 & majors50>0][order(d_vec)][1, id]
    temps = temps + d_min/vel + 90
    last_node=fcb_dir[the_min, id]
    
    if (temps > max_t) {
      estat = "new"
    } else {
      i = i+1
      rutas = rbind(rutas, data.table(id_ruta=id_ruta, node=fcb_dir[the_min, id],
                                      x=fcb_dir[the_min, x], y=fcb_dir[the_min, y],
                                      temps=temps))
      fcb_dir[id==last_node, done:=1]
    }
  }
}

rutas = merge(rutas, fcb_dir[, .(node=id, N, majors50, dir, portes)], by="node")
rutas[, comerc:= as.numeric(portes=="")]
rutas = rutas[order(id_ruta, temps)]
rutas[, n_comercos:= cumsum(comerc), by=id_ruta]
rutas = rutas[n_comercos <= 4 | comerc==0]
resum = rutas[, .(N=sum(N)), by=id_ruta][order(-N)]
resum[1:20]

data1[, pis:= paste0(planta, " ", porta)]
data1[, tengui:= as.numeric(id1 %in% fcb$id1)]

saveRDS(rutas, "data/rutas.RDS")

rutas_final = data.table()
for (i in 1:20) {
  print(i)
  this_ruta = rutas[id_ruta==resum[i, id_ruta]]
  this_ruta[, busties:= ""]
  for (j in 1:nrow(this_ruta)) {
    print(j)
    n = this_ruta[j, str_count(portes, ";")] + 1
    extra = ifelse(n<4, n*2, n)
    
    if (this_ruta[j, portes!=""]) {
      this_edifici = data1[dir==this_ruta[j, dir], .(tengui=max(tengui)),by=.(planta, porta, pis)]
      this_edifici[, do:= tengui]
      this_edifici[, do:= ifelse(do==1, do, as.numeric(shift(tengui,1)==1))]
      this_edifici[, do:= ifelse(do==1, do, as.numeric(this_edifici$tengui[2:(nrow(this_edifici)+1)]==1))]
      this_edifici[is.na(do), do:= tengui]
      
      this_ruta[j, busties:= this_edifici[do==1, paste0(pis, collapse="; ")]]
    }
  }
  rutas_final = rbind(rutas_final, this_ruta)
}

rutas_final = merge(rutas_final, fcb_dir[, .(node=id, empresa)], by="node")
rutas_final = merge(rutas_final, fcb_dir[, .(node=id, lat, lon)], by="node")

rutas_final[is.na(empresa), empresa:= ""]
saveRDS(rutas_final, "data/rutas_final.RDS")

# guardar XL -----
library(openxlsx)
wb=openxlsx::createWorkbook()
for(i in 1:20) {
  this_ruta = rutas_final[id_ruta==resum[i, id_ruta]]
  
  openxlsx::addWorksheet(wb, paste0("R",i))
  openxlsx::setColWidths(wb, paste0("R",i), cols = 1:4, widths=c(4,6,40,40))
  openxlsx::writeDataTable(wb, paste0("R",i), this_ruta[order(temps),
                                                        .(id=rank(temps), temps=round(temps),
                                                          direccio=paste0(empresa, ifelse(nchar(empresa)>0, "; ", ""), dir),
                                                          busties)], startRow=1, startCol=1, rowNames = FALSE)
}
openxlsx::saveWorkbook(wb, "data/Rutas.xlsx", overwrite = TRUE)


# ----
library(leaflet)
library(dplyr) 
library(leaflet)
library(htmlwidgets)
library(webshot)

rutas_final[, id:=as.character(rank(temps)), by=id_ruta]
# rutas_final[as.numeric(id)%%5!=1, id:=""]

rutas_final = rutas_final[order(id_ruta, temps)]
for(i in 1:20) {
  this_ruta = rutas_final[id_ruta==resum[i, id_ruta]]
  
  points = st_as_sf(this_ruta, coords=c('lon','lat'), crs=4326)
  lines = points %>% summarise(do_union=FALSE) %>% st_cast("LINESTRING") # convert to one line
  
  mapa = lines %>% select() %>% leaflet(options = leafletOptions(zoomControl = FALSE)) %>%
    htmlwidgets::onRender("function(el, x) {
        L.control.zoom({ position: 'topright' }).addTo(this)
    }") %>% 
    addPolylines(weight=3) %>%
    addTiles() %>%
    addCircles(data=this_ruta[empresa==""], lng=~lon,lat=~lat, color = "blue", radius = 3,
               label=~id, labelOptions =
                 labelOptions(noHide = T, textOnly = TRUE, offset=c(0,-12), direction="top",
                              style = list("color" = "blue", "font-size" = "20px")
                 ))
  
  if (this_ruta[empresa!="", .N>0]) {
    mapa = mapa %>% 
      addCircles(data=this_ruta[empresa!=""], lng=~lon,lat=~lat, color = "red", radius = 3,
                 label=~id, labelOptions =
                   labelOptions(noHide = T, textOnly = TRUE, offset=c(0,-12), direction="top",
                 style = list("color" = "red", "font-size" = "20px")))
  }
  
  ## save html to png
  saveWidget(mapa, "temp.html", selfcontained = FALSE)
  webshot("temp.html", file = paste0("data/R",i,".png"),
          cliprect = "viewport")
}

# ----
resum = rutas_final[, .(N=sum(N), busties=sum(str_count(busties, ";")+1-comerc), comercs = sum(comerc)), by=id_ruta][order(-N)]
resum[, id2:=1:20]
resum
library(arcvitoolbox)
write.excel(resum)

rutas_final[id_ruta==2]
rutas_final[id_ruta==15, .(id_ruta, temps, dir)]

resum[, sum(busties)]
resum[, sum(N)]


# end ----