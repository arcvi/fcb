library(data.table)

fcb = fread("data/fcb_full_join.csv")

v = fcb[, .(clientid=paste0(id,"xyz"),address=ifelse(is.na(id1), paste0(direccion, codpostal), full_dir),
                    postal_code=ifelse(is.na(id1),codpostal,cp))]

to_geoloc[grepl("NULL", address), .N]

write.csv(
  to_geoloc[105001:.N],
  'E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/code/geoloc/scraper/test_geoloc1.csv',
  fileEncoding='UTF-8') # Aquí acaba!

# workon testgeoloc
# cd /mnt/e/DOCS/Arcvi/SCA/SCA_Asignacion_2018/code/geoloc/scraper/
# 
# export KEY_GOOGLEMAPS=AIzaSyBDb_Iyv9Iu82FMUYa2Aj-QmuB3yvduJss
# scrapy crawl geoloc -a filename=test_geoloc1.csv
# 
#   Teixi: AIzaSyAcMZV11hCSNzE5wh-2I4x1W1TlD_uAzPA
# Oriol - project medevac: AIzaSyCmWrPGzALH27i-LH_hiFy0hBggjdKXd1s
# Oriol - archer drinks: AIzaSyDIIRGwaNyB-NgRv5s0_OetHHv3FV2Z3fU
# Oriol - project medevac: AIzaSyBDb_Iyv9Iu82FMUYa2Aj-QmuB3yvduJss

library(RSQLite)
library(dplyr)
con = dbConnect(drv=SQLite(), dbname="E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/code/geoloc/scraper/data.sqlite")
dbGetQuery(con,"SELECT clientid, lat, lon, google_types, google_address, postal_code, postal_code_address FROM scrapy_GeolocItem") %>% data.table -> scraped_clientid

scraped_clientid = scraped_clientid[grepl("xyz", clientid)]
scraped_clientid[is.na(google_types), google_types:= "NA"]

calle_numero = c('["street_address"]', '["premise"]', '["subpremise"]')
scraped_clientid[, redo:= as.numeric(is.na(google_types) | !google_types %in% calle_numero)]
scraped_clientid[, do:= as.numeric(is.na(google_types))]
summary(scraped_clientid)



ind_cp = fread('E:/DOCS/Arcvi/SCA/SCA_Asignacion_2018/data/ind_cp.csv')
ind_cp = unique(ind_cp, by = "cp")

scraped_clientid = merge(scraped_clientid[, .(id=gsub("xyz", "", clientid), lat, lon, mala_geoloc=redo, google_address, cp_google=postal_code,
                                              cp=as.numeric(postal_code_address))],
                         ind_cp[, .(cp, lat_cp=lat, lon_cp=lon)],
                         by = "cp", all.x=T)
scraped_clientid

scraped_clientid[, lat:=as.numeric(lat)]
scraped_clientid[, lon:=as.numeric(lon)]

scraped_clientid[, lat:= ifelse(mala_geoloc==1, lat_cp, lat)]
scraped_clientid[, lon:= ifelse(mala_geoloc==1, lon_cp, lon)]

summary(scraped_clientid)

write.csv(scraped_clientid, "data/fcb_geoloc.csv", row.names=F)

scraped_clientid
