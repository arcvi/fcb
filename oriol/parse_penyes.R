library(jsonlite)
library(data.table)
library(sf)
library(geosphere)
library(maptools)
library(rgdal)
library(readr)

str = readChar("data/penyes.json", file.info("data/penyes.json")$size)
ls = str_split(str, "\\},\\{")
vec = unlist(ls)

dt = data.table(vec = vec)
dt[, lat:= as.numeric(gsub("[^.0-9]", "", gsub("^.*lat", "", vec)))]
dt[, lon:= as.numeric(gsub("[^.0-9]", "", gsub("lat.*$", "", vec)))]

dt[, id_penya:=1:.N]

# funcions ----
spatialMerge_new = function(X_data, # should contain numeric columns called latitude and longitude
                            Y_data, # should contain numeric columns called latitude and longitude
                            CUTOFF=15000 # cutoff distance to keep results
){
  
  Y_data = copy(Y_data)
  setnames(Y_data,'latitude','latitude.y')
  setnames(Y_data,'longitude','longitude.y')
  Y_data = Y_data[!is.na(longitude.y) & !is.na(latitude.y)]
  Y_data[,id.y:=1:nrow(Y_data)]
  Y_data_sp = SpatialPoints(Y_data[,.(latitude=latitude.y,longitude=longitude.y)],
                            CRS('+init=epsg:4326'))
  
  X_unique = X_data[,.(latitude,longitude)][,unique(.SD)][!is.na(latitude) & !is.na(longitude)]
  X_unique[,id.x:=1:nrow(X_unique)]
  X_unique_sp = SpatialPoints(X_unique[,.(latitude,longitude)],
                              CRS('+init=epsg:4326'))
  
  Y_data_sp_utm = spTransform(Y_data_sp,CRS('+init=epsg:2154'))
  P_utm = rgeos::gBuffer(Y_data_sp_utm,width = 2*CUTOFF, byid=TRUE)
  P_wsg = spTransform(P_utm,CRS('+init=epsg:4326'))
  out_list = over(X_unique_sp,P_wsg,returnList=T)
  print(3)
  merged_dt=data.table(id.y=unlist(out_list))
  merged_dt$id.x = as.numeric(rep(names(out_list),times=sapply(out_list,length)))
  print(4)
  merged_data = merge(X_data,
                      merge(X_unique, 
                            merge(merged_dt,Y_data,by='id.y',all.x=T)
                            ,by='id.x',all.x=T),
                      by=c('latitude','longitude'),
                      all.x=T,
                      allow.cartesian=T)
  
  
  merged_data[,dist:=distHaversine(.SD[,.(longitude,latitude)],
                                   .SD[,.(longitude.y,latitude.y)])]
  print(5)
  merged_data[,c('id.x','id.y'):=NULL]
  
  merged_data = merged_data[dist<=CUTOFF]
  
  return(merged_data)
}
#Test funciones ----
fcb_geoloc = fread("data/fcb_geoloc.csv")
fcb_geoloc = fcb_geoloc[!is.na(lon+lat)]

x = spatialMerge_new(dt[, .(id_penya, longitude=lon, latitude=lat)],
                     fcb_geoloc[, .(id_soci=id, longitude=lon, latitude=lat)], CUTOFF = 10000)

x[, r:= rank(dist, ties.method = "first"), by=id_soci]

y = x[, .(socis_1000m=sum(dist < 1000), socis_mes_propera=sum(r==1)), by=id_penya]
y = merge(y, dt[, .(id_penya, lat, lon)], by="id_penya")

write.excel(y, dec=",")
# end ----