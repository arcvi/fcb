library(data.table)
library(geosphere)
library(futile.logger)
library(sf)
library(geosphere)
library(maptools)
library(rgdal)
library(readr)
library(dplyr)
library(arcvitoolbox)

fcb_geoloc = fread("data/fcb_geoloc.csv")

top_zones = readRDS('data/socis_cp.RDS')
top_zones[, num_punts:= round(N/1000)]

top_comercos = readRDS("data/cargos_actuales.rds")
top_comercos[, lat:= coalesce(lat_e, lat_s, lat_p)]
top_comercos[, lon:= coalesce(lon_e, lon_s, lon_p)]

top_comercos[, dir:= ifelse(!is.na(lat_e) & (lat==lat_e), ad_e, direccio)]
top_comercos[, cp:= ifelse(!is.na(lat_e) & (lat==lat_e), cp_e,
                            ifelse(!is.na(lat_s) & (lat==lat_s), cp_s, cp_p))]

top_comercos[, sum(is.na(dir))]

# funcions ----

for(i in 1:10) {
  top_zones[[paste0("comerç_",i)]] = ""
}

for (i in 1:nrow(top_zones)) {
  if (top_zones[i, cp] %in% top_comercos$cp) {
    parades_cp = top_comercos[cp==top_zones[i, cp]][order(o)]
    parades_cp =unique(parades_cp, by="empresa")
    for (j in 1:pmin(10, top_zones[i, num_punts*2])) {
      if (parades_cp[, .N] > 0) {
        if (j <= nrow(parades_cp)) {
          top_zones[i][[paste0("comerç_",j)]] = parades_cp[j, paste0(empresa, ", ", dir)]
        }
      }
    }
  }
}
write.excel(top_zones)



top_comercos[grepl('girona', tolower(dir))][order(o), .(paste0(empresa, ", ", dir))]
