library(ngram)

ngramsDT = function( str, N = 3 ) {
  str = tolower( str )
  nx = ngram( gsub("", " ", str) , n=N )
  dt = data.table( ngram = get.ngrams( nx ) )
  dt = dt[, .N, by = ngram ]
  dt
}

distNgrams = function( str1, str2, mode="union" ) {
  tryCatch({
    dt1 = ngramsDT( str1 )
    dt2 = ngramsDT( str2 )
    
    dt = merge( dt1, dt2, by = "ngram", all = T )
    dt[is.na(N.x), N.x:= 0]
    dt[is.na(N.y), N.y:= 0]
    
    union = dt[, sum( pmax( N.x, N.y ) )]
    smallest = dt[, pmin(sum(N.x), sum(N.y))]
    intersection = dt[, sum( pmin( N.x, N.y ) )]
    if (mode=="union"){
      return( intersection / union )
    } else {
      return( intersection / smallest )
    }
  }, warning = function(warning_condition) {
  }, error = function(error_condition) {
    return(0)
  }, finally={
  })
}
