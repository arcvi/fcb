"""
This script file extract tweets from the timeline of some targeted politician of the major
political parties of Ghana, it extracts the 1000 tweets at each run. and another 1000 from
the timelines of the targeted individuals
"""

import tweepy
from tweepy import Cursor
import unicodecsv
from unidecode import unidecode
import time
import pandas as pd

# Authentication and connection to Twitter API.
consumer_key = "HZ327iegbvHriO76NkPDGvkaj"
consumer_secret = "fyyizxqncXIvXXfs3AiYjPHg2OQ7VeVnOp272c8zJl0NQqFOjF"
access_key = "728238774459748352-W3ZJodQrtfRPHzm4gtf1ajMY5iWrFMl"
access_secret = "nlXhYZycG8I2IXmehpGet9uys7oON6MMblSXAW5esoclV"


auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
# api = tweepy.API(auth)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

# insert the keyword here for the extraction to continue
searchQuery = ''
retweet_filter = '-filter:retweets'

# append the term to search parameters
q = searchQuery + retweet_filter
tweetsPerQry = 100
fName = 'tweets.txt'
sinceId = None

max_id = -1
maxTweets = 1000

tweetCount = 0

socis = pd.read_csv("data/targets/socis.csv")
users = socis.V1.tolist()

# Usernames whose tweets we want to gather.
# users = [
#     "@NAkufoAddo",
# ]

secs_btw_pages = 1

# giving the user some feed back that the script is running
print("Downloading tweets from user timelines...")

# extract tweets from timeline of targeted politicians of the major political parties
with open('data/tweets.csv', 'ab') as file:
    writer = unicodecsv.writer(file, delimiter=',', quotechar='"')
    # Write header row.

    # write the titles for each column
    writer.writerow([
            "Tweet Date",
            "Tweet ID",
            "Tweet Text",
            "tweet_source",
            "tweet_retweet_count",
            "tweet_favorite_count"
    ])

    # loop through all the users and extract tweets from their relative timelines
    for user in users:
        try:
            time.sleep(secs_btw_pages)
            user_obj = api.get_user(user)

            # Get 1000 most recent tweets for the current user.
            for tweet in Cursor(api.user_timeline, screen_name=user, count=100).items(100):
                # Latitude and longitude stored as array of floats within a dictionary.
                lat = tweet.coordinates['coordinates'][1] if tweet.coordinates is not None else None
                long = tweet.coordinates['coordinates'][0] if tweet.coordinates is not None else None
                # If tweet is not in reply to a screen name, it is not a direct reply.
                direct_reply = True if tweet.in_reply_to_screen_name != "" else False
                # Retweets start with "RT ..."
                retweet_status = True if tweet.text[0:3] == "RT " else False

                # Get info specific to the current tweet of the current user.
                tweet_text = unidecode(tweet.text)

                # format the date
                tweet_date = str(tweet.created_at.year) + "/" + str(tweet.created_at.month) + "/" + str(
                    tweet.created_at.day)

                tweet_source = tweet.source

                retweet_count = tweet.retweet_count

                favorite_count = tweet.favorite_count

                tweet_id = tweet.id

                # Below entities are stored as variable-length dictionaries, if present.
                hashtags = []
                hashtags_data = tweet.entities.get('hashtags', None)
                if hashtags_data is not None:
                    for i in range(len(hashtags_data)):
                        hashtags.append(unidecode(hashtags_data[i]['text']))

                # get all urls of a tweet
                urls = []
                urls_data = tweet.entities.get('urls', None)
                if urls_data is not None:
                    for i in range(len(urls_data)):
                        urls.append(unidecode(urls_data[i]['url']))

                # get all mentions
                user_mentions = []
                user_mentions_data = tweet.entities.get('user_mentions', None)
                if user_mentions_data is not None:
                    for i in range(len(user_mentions_data)):
                        user_mentions.append(unidecode(user_mentions_data[i]['screen_name']))

                # get all media types
                media = []
                media_data = tweet.entities.get('media', None)
                if media_data is not None:
                    for i in range(len(media_data)):
                        media.append(unidecode(media_data[i]['type']))

                # get all contributors
                contributors = []
                if tweet.contributors is not None:
                    for contributor in tweet.contributors:
                        contributors.append(unidecode(contributor['screen_name']))

                # Write the extracted tweets to CSV.
                writer.writerow([
                    tweet_date, tweet_id, tweet_text, tweet_source,
                    retweet_count, favorite_count,
                ])

            # Show progress.
            print("Wrote tweets of %s to CSV." % user)
        except tweepy.TweepError as e:
            print("There was an error, find details below, else check your internet connection or your " +
                  " credentials in the credentials.py file \n")
            print("If this is not your first time running this particular script, then there is a possibility that the "
                  "maximum rate limit has been exceeded. wait a few more minutes and re run the script\n")
            print(f"Error Details: {str(e)}")


