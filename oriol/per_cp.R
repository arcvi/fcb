library(readxl)
library(dplyr)
library(data.table)
library(arcvitoolbox)
fcb = read_excel("data/FCB.xlsx") %>% data.table()
setnames(fcb, tolower(names(fcb)))
summary(fcb)
names(fcb)
fcb = unique(fcb, by=c("nombre", "apellidos"))

fcb[, prov:= floor(as.numeric(codpostal)/1000)]
fcb[is.na(prov), .N, by=idprovincia][order(-N)]
fcb[is.na(prov), prov:=as.numeric(idprovincia)]

# codpostal ciudad idprovincia idsede
# fcb[is.na(prov), .N, by=codpostal]
# fcb[is.na(prov), .N, by=ciudad]
# fcb[is.na(prov), .N, by=idprovincia]
# fcb[is.na(prov), .N, by=idsede]
# fcb[is.na(prov), .N, by=codpostal]
# fcb[is.na(prov), .N, by=substring(telefono, 1, 2)][order(-N)]
# fcb[is.na(prov), .N, by=direccion]

dic_prov = readRDS('data/ZoneDictionary.RDS')
dic_prov[, prov:=  floor(as.numeric(cp)/1000)]
dic_prov = unique(dic_prov[, .(prov, zona)], by=NULL)

fcb_prov = fcb[, .N, by=prov]
fcb_prov = merge(fcb_prov, dic_prov, by="prov")
write.excel(fcb_prov)


ind_cp = fread('data/ind_cp.csv')

fcb[, codpostal:= as.numeric(codpostal)]
fcb[, prov:= floor(codpostal/1000)]
fcb[, .N, by=.(codpostal, prov)][order(-N)]

fcb = merge(fcb, ind_cp[, .(codpostal=cp, municipi=municipio, poblacio=poblacion_total)], by="codpostal", all.x=T)
fcb[is.na(poblacio), .N, by=codpostal][order(-N)]
fcb = fcb[!is.na(poblacio)]

top_cps = fcb[, .N, by=.(codpostal, municipi)][order(-N)]
top_munis = fcb[, .N, by=.(municipi, prov)]
top_munis[prov!=8, N:= round(1.93*N)]

top_zones = rbind(top_cps[, .(zona=paste0(municipi,"-",codpostal), cp=codpostal, N)],
                  top_munis[!municipi %in% top_cps[N>500]$municipi, .(zona=municipi, cp="", N)])

top_zones = top_zones[order(-N)][N>500]

saveRDS(top_zones, 'data/socis_cp.RDS')

# ----
library(data.table)
library(dplyr)
library(dtplyr)
library(nngeo)
library(sf)
library(stringr)
library(readr)

estacions = data.table(read_delim("data/estacions.csv", delim=",", locale=locale(encoding='UTF-8')))
parades = data.table(read_delim("data/parades.csv", delim=",", locale=locale(encoding='UTF-8')))


estacions[, coords:= gsub("POINT \\(", "", GEOMETRY)]
estacions[, coords:= gsub("\\)", "", coords)]
estacions[, lon:= as.numeric(gsub(" .*$", "", coords))]
estacions[, lat:= as.numeric(gsub("^.* ", "", coords))]


estacions[, .N, by=NOM_ESTACIO ][order(-N)]
estacions[, n:= nchar(PICTO)]
estacions[order(-n)]

parades[, .N, by=NOM_TIPUS_SIMPLE_PARADA]
parades[, coords:= gsub("POINT \\(", "", GEOMETRY)]
parades[, coords:= gsub("\\)", "", coords)]
parades[, lon:= as.numeric(gsub(" .*$", "", coords))]
parades[, lat:= as.numeric(gsub("^.* ", "", coords))]

parades[NOM_TIPUS_SIMPLE_PARADA=="Marquesina"]

parades_estacions = rbind(
  estacions[, .(nom=NOM_ESTACIO, tipus="metro", id=paste0("metro-",CODI_GRUP_ESTACIO), linies=PICTO, lat, lon)],
  parades[NOM_TIPUS_SIMPLE_PARADA=="Marquesina ", .(nom=NOM_PARADA, tipus="bus", id=paste0("bus-",ID_PARADA), linies="", lat, lon)]
)

cp_map = read_sf('E:/DOCS/Arcvi/arcvi-maps/data/CP-Spain.kml')
catastro_coord = st_crs(cp_map)


parades_sf = st_as_sf(parades_estacions %>% as.data.frame(), coords = c("lon", "lat"),
                      crs = 4326, agr = "constant") %>%
  st_transform(catastro_coord)


parades_sf %>%
  st_join(cp_map %>% st_transform(catastro_coord) %>% select(CP=Name), left=TRUE) %>%
  as.data.frame() %>% select(id, CP) -> aux

parades_estacions = merge(parades_estacions, data.table(aux), by="id")

fcb_geoloc = fread("data/fcb_geoloc.csv")

fcb_geoloc = fcb_geoloc[!is.na(lon+lat) & mala_geoloc==0]

DT_sf = st_as_sf(fcb_geoloc, coords = c("lon", "lat"),
                 crs = 4326, agr = "constant") #4326 es el sistema lat lon de google

DT_utm = st_transform(DT_sf, 25831) # aixo es UTM31 són metres centrats a Barcelona mes o menys

DT_utm = data.table( st_coordinates(DT_utm) ) # aixo dona les coordenades en matriu per que les puguis ficar on vulguis

fcb_geoloc$x = DT_utm$X
fcb_geoloc$y = DT_utm$Y

fcb_geoloc = fcb_geoloc[!is.na(x+y)]
# funcions ----
library(data.table)
library(geosphere)
library(futile.logger)
library(sf)
library(geosphere)
library(maptools)
library(rgdal)
library(readr)

spatialMerge_new = function(X_data, # should contain numeric columns called latitude and longitude
                            Y_data, # should contain numeric columns called latitude and longitude
                            CUTOFF=15000 # cutoff distance to keep results
){
  
  Y_data = copy(Y_data)
  setnames(Y_data,'latitude','latitude.y')
  setnames(Y_data,'longitude','longitude.y')
  Y_data = Y_data[!is.na(longitude.y) & !is.na(latitude.y)]
  Y_data[,id.y:=1:nrow(Y_data)]
  Y_data_sp = SpatialPoints(Y_data[,.(latitude=latitude.y,longitude=longitude.y)],
                            CRS('+init=epsg:4326'))
  
  X_unique = X_data[,.(latitude,longitude)][,unique(.SD)][!is.na(latitude) & !is.na(longitude)]
  X_unique[,id.x:=1:nrow(X_unique)]
  X_unique_sp = SpatialPoints(X_unique[,.(latitude,longitude)],
                              CRS('+init=epsg:4326'))
  
  Y_data_sp_utm = spTransform(Y_data_sp,CRS('+init=epsg:2154'))
  P_utm = rgeos::gBuffer(Y_data_sp_utm,width = 2*CUTOFF, byid=TRUE)
  P_wsg = spTransform(P_utm,CRS('+init=epsg:4326'))
  out_list = over(X_unique_sp,P_wsg,returnList=T)
  print(3)
  merged_dt=data.table(id.y=unlist(out_list))
  merged_dt$id.x = as.numeric(rep(names(out_list),times=sapply(out_list,length)))
  print(4)
  merged_data = merge(X_data,
                      merge(X_unique, 
                            merge(merged_dt,Y_data,by='id.y',all.x=T)
                            ,by='id.x',all.x=T),
                      by=c('latitude','longitude'),
                      all.x=T,
                      allow.cartesian=T)
  
  
  merged_data[,dist:=distHaversine(.SD[,.(longitude,latitude)],
                                   .SD[,.(longitude.y,latitude.y)])]
  print(5)
  merged_data[,c('id.x','id.y'):=NULL]
  
  merged_data = merged_data[dist<=CUTOFF]
  
  return(merged_data)
}
#Test funciones ----

x = spatialMerge_new(parades_estacions[, .(id_parada=id, longitude=lon, latitude=lat)],
                     fcb_geoloc[, .(id, longitude=lon, latitude=lat)], CUTOFF = 500)

y = x[, .(socis_500m=.N), by=id_parada]
parades_estacions = merge(parades_estacions, y[, .(id=id_parada, socis_500m)], by="id", all.x=T)


x = spatialMerge_new(parades_estacions[, .(id_parada=id, longitude=lon, latitude=lat)],
                     parades_estacions[, .(id, longitude=lon, latitude=lat)], CUTOFF = 200)

y = x[, .(estacions_200m=.N), by=id_parada]
parades_estacions = merge(parades_estacions, y[, .(id=id_parada, estacions_200m)], by="id", all.x=T)
parades_estacions[, cp:= as.numeric(CP)]
# best spots ----
top_zones[, spot1:=""]
top_zones[, spot2:=""]
top_zones[, spot3:=""]

parades_estacions[, score:= socis_500m^0.5*estacions_200m^0.5]

for (i in 1:nrow(top_zones)) {
  if (top_zones[i, cp] %in% parades_estacions$cp) {
    parades_cp = parades_estacions[cp==top_zones[i, cp]]
    for (j in 1:3) {
      if (parades_cp[, .N] > 0) {
        parades_cp = parades_cp[order(-score)]
        top_zones[i][[paste0("spot",j)]] = parades_cp[1, paste0(nom, " (parada ", tipus, ")")]
        
        parades_cp[, dist:= distHaversine(.SD[, .(lon, lat)], .SD[, .(parades_cp[1, lon], parades_cp[1, lat])])]
        parades_cp = parades_cp[dist > 500]
      }
    }
  }
}
write.excel(top_zones)
# ----

# cp_map = read_sf('E:/DOCS/Arcvi/arcvi-maps/data/CP-Spain.kml')
# catastro_coord = st_crs(cp_map)
# 
# 
# fcb_sf = st_as_sf(fcb_geoloc %>% as.data.frame(), coords = c("lon", "lat"),
#                       crs = 4326, agr = "constant") %>%
#   st_transform(catastro_coord)
# 
# fcb_sf %>%
#   st_join(cp_map %>% st_transform(catastro_coord) %>% select(CP=Name), left=TRUE) %>%
#   as.data.frame() %>% select(id, CP) -> aux
# 
# fcb_geoloc = merge(fcb_geoloc, data.table(aux), by="id", all.x=T)
# fcb_geoloc[, cp:= as.numeric(cp)]

fcb_geoloc[, cp:= cp_google]
fcb_geoloc = merge(fcb_geoloc, ind_cp[, .(cp, municipi=municipio)], by="cp")

missing_zones = top_zones[spot1==""]

for (i in 1:nrow(missing_zones)) {
  if (missing_zones[i, cp==""]) {
    socis_zona = fcb_geoloc[municipi == missing_zones[i,zona] ]
    matrix = as.matrix(socis_zona[, .(x, y)])
  } else {
    socis_zona = fcb_geoloc[cp == missing_zones[i,cp]]
    matrix = as.matrix(socis_zona[, .(x, y)])
  }
  centres = data.table(kmeans(matrix, 3)$centers)
  
  for(j in 1:3) {
    socis_zona[, dist:= sqrt((x-centres[j,x])^2 + (y-centres[j,y])^2)]
    missing_zones[i][[paste0("spot",j)]] = socis_zona[order(dist)][1, google_address]
  }
}
write.excel(missing_zones)

top_zones= rbind(top_zones[!zona %in% missing_zones$zona],
                 missing_zones)
write.excel(top_zones)

# end ----