# -*- coding: utf-8 -*-
import pandas as pd
import re

import time
import ndjson
import tweepy
import logging

def get_logger():
    logger = logging.getLogger('get_followers_ids')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('log.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger


def get_api_external():
    auth = tweepy.OAuthHandler("HZ327iegbvHriO76NkPDGvkaj", "fyyizxqncXIvXXfs3AiYjPHg2OQ7VeVnOp272c8zJl0NQqFOjF")
    print(auth.get_authorization_url())
    pin = input('Verification pin number from twitter.com: ').strip()
    token = auth.get_access_token(verifier=pin)
    api = tweepy.API(auth)
    logger.info("api auth OK")
    return api

logger = get_logger()


leads = pd.read_csv('target20210111_300.csv',encoding='utf8',sep=';').fillna('').to_dict('records')

template="""
Hola{}! 

Com saps, el Barça viu una tempesta perfecta plena de grans reptes i dificultats. Per això, la paraula dels socis i les sòcies és més necessària que mai. Si vols aprofitar aquesta oportunitat històrica i convertir el Barça en un Club imbatible, dóna'ns el teu suport! 
Tens temps fins les 15h! Et recollim la signatura a casa: https://sialfutur.cat/suma/ 

Construïm plegats el Barça del futur!
"""

api = get_api_external()

def send_dm(screen_name:str, name: str, **kwargs):
    logger.info("Sending DM to {} with name {}".format(screen_name, name))
    if len(name)>0:
        name = " "+name
    text = template.format(name)

    user = api.get_user(screen_name)
    api.send_direct_message(user.id, text)
    logger.info("Sended DM to {} with name {}".format(screen_name, name))

#leads = [{"name":"Miguel", "screen_name":"@MTeixido_"}]

for lead in leads:
    try:
        send_dm(**lead)
        time.sleep(66)
    except:
        logger.error("Error with lead {}".format(lead))
        logger.exception("Unexpected error")
        time.sleep(65)

