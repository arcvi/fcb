import pandas as pd
import re

def get_to_download():
    all_file = open('sialfutur.csv').read()

    all_file2 = re.sub('([0-9])@','\\1\n@',all_file)

    lines = all_file2.split('\n')

    data = []
    for line in lines:
        tokens = line.split(',')
        if not tokens[1].strip().isdigit():
            print(line)
            break
        tokens = line.split(',')
        data.append((tokens[0].strip(),int(tokens[1].strip())))

    X = pd.DataFrame(data)
    X.columns = ['page','id']

    ids = X.groupby('id').count().sort_values('page',ascending=False).reset_index().id.to_list()

    ids = ids[0:2000]
    return ids

import time
import ndjson
import tweepy
import logging

def get_logger():
    logger = logging.getLogger('get_followers_ids')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('log.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger

def get_api():
    x = tweepy.OAuthHandler("HZ327iegbvHriO76NkPDGvkaj", "fyyizxqncXIvXXfs3AiYjPHg2OQ7VeVnOp272c8zJl0NQqFOjF")
    x.secure = True
    x.set_access_token("728238774459748352-Q4Uptujp2CzkuCbiBl5M9BCnVhdFrzF", "v14umf6VbkNcrrsKBeQSKyZpm3eGCzOoSUUfAwzALvIXB")
    api = tweepy.API(x)
    logger.info("api auth OK")
    return api


def lookup_users(api, writer, ids):
    logger.info("calling 100 ids")
    result = api.lookup_users(user_ids=ids[0:100])    
    for r in result:
        writer.writerow(r._json)
    logger.info("saved 100 ids")
    fout.flush()
    time.sleep(3)

def get_downloaded_ids():
    readed_ids = []
    with open('followers.ndjson') as f:
        reader = ndjson.reader(f)
        for row in reader:
            if 'id' in row:
                readed_ids.append(row['id'])

    readed_ids = set(readed_ids)
    logger.info("Readed {} ids already".format(len(readed_ids)))
    return readed_ids

logger = get_logger()

api = get_api()

fout = open('followers.ndjson','a')
writer = ndjson.writer(fout)


ids = get_to_download()

readed_ids = get_downloaded_ids()
ids = [id for id in ids if id not in readed_ids] # filter already downloaded

logger.info("Remaining {} ids".format(len(ids)))

chunks = [ids[x:x+100] for x in range(0, len(ids), 100)]
for i, chunk in enumerate(chunks):
    lookup_users(api, writer, chunk)
    logger.info("done chunk {}".format(i))
fout.close()

logger.info("Done {} ids".format(len(ids)))

##########

