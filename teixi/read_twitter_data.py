import gzip
import json
import logging
import pandas as pd
from unidecode import unidecode

def get_logger():
    logger = logging.getLogger('get_followers_ids')
    logger.setLevel(logging.DEBUG)
    # create file handler which logs even debug messages
    fh = logging.FileHandler('log.txt')
    fh.setLevel(logging.DEBUG)
    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    fh.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    logger.addHandler(fh)
    return logger

logger=get_logger()


fields = ['id','screen_name','location','name','description','followers_count', 'friends_count','created_at']

def get_fields(d):
    return {k:d[k] for k in fields if k in d}

all_data = []
logger.info("Starting file load")
with open('followers.ndjson') as f:
    for i, line in enumerate(f.readlines()):
        if i % 5000==0:
            logger.info("Processed {}".format(i))
        #print(line)
        data = json.loads(line)
        if 'id' in data:
            #print(get_fields(data))
            all_data.append(get_fields(data))
        #if i>50000:
        #    break

twitters = pd.DataFrame(all_data)
twitters['clean_name'] = twitters['name'].apply(lambda s: unidecode(s.lower()))
twitters = twitters.reset_index()
